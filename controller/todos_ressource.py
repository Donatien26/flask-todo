from flask_restful import Resource
from configuration.security import token_required
from services.todo_service import TodoService


class TodosRessource(Resource):
    def get(self):
        return TodoService.get_all()
