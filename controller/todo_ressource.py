from flask import request
from flask_restful import Resource, abort
from configuration.security import token_required
from services.todo_service import TodoService


class TodoRessource(Resource):
    def get(self, id):
        todo = None
        try:
            todo = TodoService.get(id)
        except:
            TodoRessource.abort_if_todo_doesnt_exist(id)
        return todo

    @token_required
    def put(self, id):
        dict_attribut = request.json
        if "task" in dict_attribut:
            TodoService.add(id, dict_attribut["task"])
            return TodoService.get(id)
        else:
            abort("la création a échouée", 500)

    @staticmethod
    def abort_if_todo_doesnt_exist(todo_id):
        return abort(404, message="Todo {} doesn't exist".format(todo_id))
