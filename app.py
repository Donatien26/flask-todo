from flask import Flask, make_response
from flask_restful import Api
from simplexml import dumps
from controller.todos_ressource import TodosRessource
from controller.todo_ressource import TodoRessource
from configuration import properties
from configuration.security import token_required

# Import variable env
app = Flask(__name__)


# On créé nos routes
api = Api(app, default_mediatype="application/json")
api.add_resource(TodosRessource, "/todos")
api.add_resource(TodoRessource, "/todo/<string:id>")


@api.representation("application/xml")
def output_xml(data, code, headers=None):
    """Makes a Flask response with a XML encoded body"""
    resp = make_response(dumps({"response": data}), code)
    resp.headers.extend(headers or {})
    return resp


@app.route("/")
def hello_world():
    return "Hello, World!"


if __name__ == "__main__":
    app.run(debug=properties.DEBUG)