class TodoService:

    TODOS = {
        "todo1": {"task": "build an API"},
        "todo2": {"task": "?????"},
        "todo3": {"task": "profit!"},
    }

    @staticmethod
    def add(id, task):
        task = {"task": task}
        TodoService.TODOS[id] = task

    @staticmethod
    def get(id):
        return TodoService.TODOS[id]

    @staticmethod
    def get_all():
        return TodoService.TODOS


