from flask import request, jsonify
from functools import wraps


def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if 'authorization' in request.headers:
            token = request.headers['authorization']

        if not token:
            return jsonify({'message': 'a valid token is missing'})

        try:
            if token != "LetMeIn":
                return jsonify({'message': 'token is invalid'})
            # data = jwt.decode(token, app.config[SECRET_KEY])
            # current_user = Users.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'token is invalid'})

        return f(*args, **kwargs)

    return decorator
